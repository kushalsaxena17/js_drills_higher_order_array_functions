module.exports =function flatten(elements) {
    let arr =[];
    if(Array.isArray(elements)){
        for(let i=0;i<elements.length;i++){
            if(Array.isArray(elements[i])){
              arr = arr.concat(flatten(elements[i]));
            }else{
                arr.push(elements[i]);
            }
       }
    }
    
    
    return arr;
    
}

