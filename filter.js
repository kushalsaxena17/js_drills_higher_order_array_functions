module.exports = function filter(elements, cb) {
    let arr = [];
    if (elements && Array.isArray(elements)) {
        for (let i = 0; i < elements.length; i++) {
            let res = cb(elements[i]);
            if (res == true) {
                arr.push(elements[i]);
            }
        }
    }
    return arr;
}



