module.exports =function map(elements, cb) {
    let arr = [];
     if (elements && cb && Array.isArray(elements)) {
          for (let i = 0; i < elements.length; i++) {
               let val = cb(elements[i],i,elements);
               arr.push(val);
          }
     }
     return arr;


}
